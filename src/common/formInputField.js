import React, { Component } from 'react';
import {StyleSheet, TextInput, View} from 'react-native';

export default class FormInputField extends Component<> {

    textInput: TextInput;

    render() {
        return (
            <View style={[Styles.container, this.props.containerStyle]}>
                <TextInput
                    style={this.props.textStyle}
                    ref={(input) => this.textInput = input}
                    onFocus={this.focus}
                    onBlur={this.blur}
                    onSubmitEditing={this.props.onSubmitEditing}
                    returnKeyType={this.props.returnKeyType}
                    secureTextEntry={this.props.secureTextEntry}
                    placeholder={this.props.placeholder}
                    onChangeText={this.props.onChangeText}
                    value={this.props.value}
                />
            </View>
        );
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        alignContent: 'center',
        justifyContent: 'center',
        borderStyle: 'solid',
        minHeight: 44,
        borderRadius: 30,
        borderColor: '#d5d5d5',
        borderWidth: 2,
        paddingStart: 10,
        backgroundColor: '#ffffff',
    },
});
