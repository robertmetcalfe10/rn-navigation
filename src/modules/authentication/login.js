import React, { Component } from 'react';
import { View } from 'react-native';
import { Button } from 'react-native-elements';
import FormInputField from "src/common/formInputField";


export default class LoginScreen extends Component {

    emailInput: FormInputField;
    passwordInput: FormInputField;

    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            loading: false,
        };
    }

    componentDidMount() {
    }

    render() {
        return (
                <View style={{margin: 20}}>
                        <FormInputField
                            testLabel={'emailField'}
                            ref={input => (this.emailInput = input)}
                            placeholder="Email"
                            value={this.state.email}
                            returnKeyType={'next'}
                            onChangeText={this.onChangeEmail}
                            blurOnSubmit={false}
                        />
                        <FormInputField
                            testLabel={'passwordField'}
                            ref={input => (this.passwordInput= input)}
                            placeholder="Password"
                            value={this.state.password}
                            returnKeyType={'done'}
                            secureTextEntry={true}
                            onChangeText={this.onChangePassword}
                            onSubmitEditing={this.onEndEditingPassword}
                        />
                    <Button
                        title={'Login'}
                        onPress={this.onPressLogin}
                    />
                </View>
        )
    }

    onChangeEmail = (value: string) => {
        this.setState({
            email: value,
        })
    };


    onChangePassword = (value: string) => {
        this.setState({
            password: value,
        })
    };

    onEndEditingPassword = () => {
        this.onPressLogin()
    };

    onPressLogin = () => {
        // if (this.emailInput) {
        //     this.emailInput.blur();
        // }
        // if (this.passwordInput) {
        //     this.passwordInput.blur();
        // }
        // this.doLogin();
        // this.navigation.navigate('Dashboard')
        this.props.navigation.navigate('Tab')
    };

    // async doLogin() {
    //     this.setState({
    //         loading: true,
    //     });
        // await this.authenticationInteractor
        //     .login(this.state.email, this.state.password)
        //     .then(() => {
        //         this.props.navigation.navigate('Dashboard');
        //     })
        //     .catch(error => {
        //         Alert.alert('Error', error.message);
        //         this.setState({
        //             loading: false,
        //         });
        //     });
    //
    // }
}
