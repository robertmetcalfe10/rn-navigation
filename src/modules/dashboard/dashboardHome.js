import React, { Component } from 'react';
import {Dimensions, View} from 'react-native';
import {Card} from 'react-native-elements';
import Carousel from 'react-native-snap-carousel';


export default class DashboardHomeScreen extends Component {

    carousel: Carousel;

    constructor(props) {
        super(props);
    }

    renderItem = ({item}) => {
        return (
            <View style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
            }}>
                <Card title={item.title}/>
            </View>
        );
    };

    render() {
        return (
            <Carousel
                ref={(c) => { this.carousel = c; }}
                data={this.ENTRIES}
                renderItem={this.renderItem}
                sliderWidth={Dimensions.get('window').width}
                itemWidth={Dimensions.get('window').width}
            />
        )
    }

    ENTRIES = [
        {
            title: 'Lorem ipsum dolor sit amet1',
        },
        {
            title: 'Lorem ipsum dolor sit amet2',
        },
        {
            title: 'Lorem ipsum dolor sit amet3',
        },
        {
            title: 'Lorem ipsum dolor sit amet4',
        },
        {
            title: 'Lorem ipsum dolor sit amet5',
        },
    ];
}
