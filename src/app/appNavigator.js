import {
    createSwitchNavigator,
    createStackNavigator,
    createBottomTabNavigator
} from "react-navigation";
import LoginScreen from "src/modules/authentication/login";
import LogoutScreen from "src/modules/authentication/logout";
import DashboardScreen from "src/modules/dashboard/dashboardHome";
import SettingsScreen from "src/modules/settings/settings";

const AuthNavigator = createStackNavigator({
        Login: {
            screen: LoginScreen
        },
        Logout: {
            screen: LogoutScreen
        }
    },
    {
        headerMode: "none",
        mode: "modal",
        cardStyle: {
            shadowOpacity: 0,
        },
    },
);

const TabNavigator = createBottomTabNavigator({
    Dashboard: {
        screen: DashboardScreen,
    },
    Settings: {
        screen: SettingsScreen,
    }
});

const MainNavigator = createSwitchNavigator({
    Auth: AuthNavigator,
    Tab: TabNavigator,
});

const AppNavigator = createStackNavigator(
    {
        Main: {
            screen: MainNavigator,
        },
    },
    {
        headerMode: "none",
        mode: "modal",
        cardStyle: {
            shadowOpacity: 0,
        },
    },
);

export default AppNavigator;
